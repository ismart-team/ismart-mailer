<?php
require_once(__DIR__ . '/idna_converver.class.php');

class Mailer
{
	private $config;
	private $mail;
	private $twig;
	private $template;
	private $templateName = "default";

	public function __construct($config, $templateName)
	{
		$this->config = $config;
		if (isset($template)) {
			$this->templateName = $templateName;
		}

		$this->init();
	}

    // Удаляет пусты поля
	private function __deleteEmptyFields(&$array)
	{
		foreach( $array as $key => $val ){
			if( is_array($val)) {
				$this->__deleteEmptyFields($array[$key]);
			} elseif( empty($val)){
				unset($array[$key]);
			}
		}
	}

    //Удаляет пустые группы полей
	private function __deleteEmptyGroup(&$array)
	{
		foreach ($array as $key => $value) {
			if( empty($value)){
				unset($array[$key]);
			}
		}
	}

	public function init()
	{
		//Преобразование кирилических доменов
		$convert = new idna_convert();
		$host = $convert->decode($_SERVER['HTTP_HOST']);

		//Инициализация шаблонизатора
		$loader = new Twig_Loader_Filesystem(__DIR__ .'/templates');
		$this->twig = new Twig_Environment($loader, array());

		$this->template = $this->templateName . ".twig";

		//Настройки SMTP-сервера
		$email = $this->config["username"];
		$this->mail = new PHPMailer();
		$this->mail->IsSMTP();
		$this->mail->SMTPAuth = true;
		$this->mail->SMTPSecure = "ssl";
		$this->mail->Host = "smtp.yandex.ru";
		$this->mail->Port = 465;
		$this->mail->Username = $this->config["username"];
		$this->mail->Password = $this->config["password"];

		$this->mail->CharSet = "utf-8";
		$this->mail->Subject = "Клиент оставил заявку на сайте " . $host;

		// Указываем отправителя
		$this->mail->SetFrom($email, $host);

		//Устанавливает получателей писем
		switch ($this->config['debug']) {
			case false:
				foreach ($this->config['address'] as $item) {
					$this->mail->AddAddress($item, "");
				}

				foreach ($this->config['addressCC'] as $item) {
					$this->mail->AddCC($item, "");
				}
				break;

			case true:
				$this->mail->AddAddress($this->config['developerAddress'], "");
		}
	}

	public function send()
	{
		$this->__deleteEmptyFields($this->config['data_mail']['fields']);
		$this->__deleteEmptyGroup($this->config['data_mail']['fields']);

		//Рендерит тело письма
		$email_body = $this->twig->render($this->template, $this->config['data_mail']);

		//Отправка почты
		$this->mail->MsgHTML($email_body);


		if (!$this->mail->send()) {
			echo 'Сообщение не было отправлено.';
			echo 'Ошибка отправления: ' . $this->mail->ErrorInfo;
			exit;
		}
	}
}
